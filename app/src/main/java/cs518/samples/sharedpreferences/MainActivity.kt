package cs518.samples.sharedpreferences

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.LinearLayout

/**
 * Sample using preferences
 * ref: http://developer.android.com/training/data-storage/shared-preferences#kotlin
 * @author PMCampbell
 * @version 2019
 *
 */
class MainActivity : Activity() {
    private var uidstr: String? = null
    private var passwdstr: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // if you need more than one
        // val sharedPref = getSharedPreferences(
        //        getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        // only one shared prefs file, so name defaults
        val prefs = getPreferences(Context.MODE_PRIVATE)

        uidstr = prefs!!.getString(getString(R.string.uid), "nothing")
        if (uidstr !== "nothing") {
            // activate welcome back message if prefs contains a userid.
            (findViewById(R.id.welcomeback) as TextView).visibility = View.VISIBLE
        }
        Log.w(TAG, "restored uid" + uidstr)

        // display on UI

        (findViewById(R.id.uidsaved) as TextView).text = uidstr
        (findViewById(R.id.resultlayout) as LinearLayout).visibility = View.VISIBLE

    }

    fun saveUserInfo(view: View) {
        // get UI  data
        uidstr = (findViewById(R.id.uid) as EditText).text.toString()
        passwdstr = (findViewById(R.id.passwd) as EditText).text.toString()
        /*
		 * logically is better to save the preferences when the data are changed
		 */

        // Store values between app instances
        // we need an editor, we are writing.
        with (getPreferences(Context.MODE_PRIVATE).edit()) {
            // set the key/value pairs
            putString(getString(R.string.uid), uidstr)
            // prefer apply() but this is a simple app
            commit()
        }
    }

    companion object {
        private val TAG = "SHPREF"
    }

}
